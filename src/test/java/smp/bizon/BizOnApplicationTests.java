package smp.bizon;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import smp.bizon.domain.bas.Address;
import smp.bizon.repository.AddressRepository;

import java.util.List;

import static org.junit.Assert.assertTrue;

@Configuration
@EnableAutoConfiguration
class SaltEdgeConfig {}

@ContextConfiguration(classes = {SaltEdgeConfig.class})
@SpringBootTest
class SaltEdge {
}


@RunWith(SpringJUnit4ClassRunner.class)
@ComponentScan(basePackages = "smp.bizon")
@EntityScan(basePackages={
		"smp.bizon.domain",
})
public class BizOnApplicationTests extends SaltEdge{

	@Autowired
	AddressRepository addressRepository;



	@Test
	public void contextLoads() {

		List<Address> list = addressRepository.findAll();

		assertTrue(list.size() != 0);
	}

}
