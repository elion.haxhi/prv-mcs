

$(document).ready(function(){



    $('#tableATMResults').DataTable( {
        "paging":   true,
        "ordering": true,
        "info":     true,
        "searching":   false
    } );


    $.ajax({
        url:'/api/address' ,
        type: 'GET',
    }).done(function(data){

        document.getElementById("loader").style.display = "none";

        fillUserTable($('#tableATMResults'), data.entities);


    }).fail(function(data) {
        alert("error");
    });


    $('#search').on('click', function() {
        $('[name="cityId"]').focus();

        document.getElementById("loader").style.display = "block";


        var argsSerialized = serializeContainer('citydiv', 'input');

        function serialize( obj ) {
            let str = '?' + Object.keys(obj).reduce(function(a, k){
                a.push(k + '=' + encodeURIComponent(obj[k]));
                return a;
            }, []).join('&');
            return str;
        }


        // Clear all table data
        clearDataTable($('#tableATMResults'));

        $.ajax({
            headers: {
                'Content-Type': 'application/json'
            },
            url: '/api/address'+serialize(argsSerialized),
            type: 'POST'
        }).done(function (response) {

            document.getElementById("loader").style.display = "none";

            $('#tableATMResults').dataTable().fnAddData([
                response.entity.street,
                response.entity.houseNumber,
                response.entity.postalCode,
                response.entity.city,
                response.entity.geolocation.lat,
                response.entity.geolocation.lng
            ]);


        }).fail(function () {
            alert("error connection")
        });
    });


     function serializeContainer(id, objecttype){
        var obj = {};


        $('#'+id).find(objecttype).each(function() {
            var value = "";

            var objName = $(this).attr('name');


            if(objName) {
                obj.city = $(this).val();
            }
        });

        return obj;
    };
    

});

function clearDataTable(objDataTable) {
    if(objDataTable.length) {
        objDataTable.dataTable().fnClearTable();

    }
}

function fillUserTable(tableObj, jSON) {
    if((jSON.length)&&(tableObj.length)) {

        for (var i = 0; i < jSON.length; i++) {

            var obj = jSON[i];

            var street = obj.street;
            var housenumber = obj.houseNumber;
            var postalcode = obj.postalCode;
            var city = obj.city;
            var latitude = obj.geolocation.lat;
            var longitude = obj.geolocation.lng;



            tableObj.dataTable().fnAddData([
                street,
                housenumber,
                postalcode,
                city,
                latitude,
                longitude,
            ]);

        }
    }
}

