
create table address
(
    id             bigserial not null,
    street       varchar(20),
    houseNumber      varchar(20),
    postalCode     varchar(20),
    city       varchar(20),
    lat        numeric (19,4),
    lng         numeric (19,6),
    primary key (id)
);

