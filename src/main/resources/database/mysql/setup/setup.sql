INSERT INTO address (id, street, housenumber, postalcode, city, lat, lng)
VALUES (1, 'Via Giovanetti', '2', '5344 AM', 'Genova', 1234.6789, 14.7589);
INSERT INTO address (id, street, housenumber, postalcode, city, lat, lng)
VALUES (2, 'Van Nesstraat', '71', '2024 DM', 'Haarlem', 52.4061, 4.645465);
INSERT INTO address (id, street, housenumber, postalcode, city, lat, lng)
VALUES (3, 'Dillenburgplein', '5-17', '2983 CB', 'Ridderkerk', 51.883958, 4.605482);
INSERT INTO address (id, street, housenumber, postalcode, city, lat, lng)
VALUES (4, 'Kaaikhof', '28A', '1567 JP', 'Assendelft', 52.488311, 4.752565);