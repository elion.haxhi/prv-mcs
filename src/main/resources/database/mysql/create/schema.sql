create table address
(
    id            BIGINT(20) NOT NULL AUTO_INCREMENT,
    street       varchar(20),
    houseNumber      varchar(20),
    postalCode     varchar(20),
    city       varchar(20),
    lat        numeric (19,4),
    lng         numeric (19,6),
    primary key (id)
);