package smp.bizon.controller;

import org.springframework.web.bind.annotation.*;
import smp.bizon.domain.bas.Address;
import smp.bizon.dto.ResponseStatusEntity;
import smp.bizon.dto.ResponseStatusList;
import smp.bizon.repository.AddressRepository;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping(value = "/api")
public class AddressController{

    AddressRepository addressRepository;

    AddressController(AddressRepository addressRepository){
        this.addressRepository = addressRepository;
    }

    @RequestMapping(value ="/address", method = RequestMethod.GET)
    public ResponseStatusList<?> getAllAddress(){
        ResponseStatusList<?> response = new ResponseStatusList<>();
        List entities = addressRepository.findAll();
        if(!entities.isEmpty()){
            response.setEntities(entities);
            response.setSuccess();
        } else{
            response.setError();
        }
        return response;
    }

    @RequestMapping(value = "/address", method = RequestMethod.POST)
    public ResponseStatusEntity<? super Address> filterByCityName(@RequestParam(name = "city") String city){
        ResponseStatusEntity<? super Address> response = new ResponseStatusEntity<>();
        Address entity =  addressRepository.findByCityEquals(city);
        if(!Objects.isNull(entity)){
            response.setEntity(entity);
            response.setSuccess();
        } else {
            response.setError();
        }
        return response;
    }
}
