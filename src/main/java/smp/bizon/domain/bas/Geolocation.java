package smp.bizon.domain.bas;

import lombok.*;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.math.BigDecimal;

@Embeddable
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Geolocation {

    @Column(name = "lat")
    private BigDecimal lat;

    @Column(name = "lng")
    private BigDecimal lng;

    @Builder(builderMethodName = "builder")
    public static Geolocation newGeoLocation(BigDecimal lat, BigDecimal lng){
        return new Geolocation(lat, lng);
    }
}
