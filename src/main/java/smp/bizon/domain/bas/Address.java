package smp.bizon.domain.bas;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "address")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "street")
    private String street;

    @Column(name = "houseNumber")
    private String houseNumber;

    @Column(name = "postalCode")
    private String postalCode;

    @Column(name = "city")
    private String city;

    private Geolocation geolocation;

    @Builder(builderMethodName = "builder")
    public static Address newAddress(
            long id,
            String street,
            String houseNumber,
            String postalCode,
            String city,
            Geolocation geolocation){
        return new Address(id,street,houseNumber,postalCode,city,geolocation);
    }
}
