package smp.bizon.domain.main;

import org.hibernate.jpa.HibernatePersistenceProvider;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TemporalType;
import javax.persistence.spi.PersistenceProvider;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

import static java.lang.System.out;

/**
 * Description utils: The META-INF folder is needed only for the purpose of working
 * with the domain model. As you see this class doesn't use tomcat and either junit
 * so it is a little project embedded inside the application.
 * When you run the test unit this class doesn't run and this is good. Here we talk with the real db
 * so be careful with the data in case when you change the state .Why i decided to do so? Because
 * it is much faster than the intergation test of junit. I talk with the database and i can create
 * the business logic.
 *
 */
public class MainAppFunction {

    static EntityManager em =null;
    static {
        System.setProperty("org.jboss.logging.provider", "jdk");// for more details comment this line
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_db_PU");
        PersistenceProvider pp = new HibernatePersistenceProvider();
        em = emf.createEntityManager();
    }


    public static void main(String[] args) {

        System.exit(0);
    }

//    public static void main36(String[] args) {
//        User e = (User) em.createNamedQuery("get_all_user").getResultList().get(0);
//        out.println(e);
//    }



//    public static void main29(String[] args) {
//        em.getTransaction().begin();
//        User user = new User();
//        user.setId(4l);
//        user.setUserName("e2");
//
//        em.persist(user);
//
//        em.getTransaction().commit();
//    }


}
