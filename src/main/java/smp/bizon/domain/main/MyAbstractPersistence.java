package smp.bizon.domain.main;


import org.hibernate.jpa.boot.internal.EntityManagerFactoryBuilderImpl;
import org.hibernate.jpa.boot.internal.PersistenceUnitInfoDescriptor;

import javax.persistence.EntityManagerFactory;
import javax.persistence.spi.PersistenceUnitInfo;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public abstract class MyAbstractPersistence {

    public static final String BAS = "smp/bizon/domain/bas";

    public EntityManagerFactory entityManagerFactory;

    {
        // for more details comment this line
        System.setProperty("org.jboss.logging.provider", "jdk");

        PersistenceUnitInfo persistenceUnitInfo = null;
        try {
            List<String> pcks = Arrays.asList(BAS);
            persistenceUnitInfo = new PersistenceUnitInfoImpl(
                    getClass().getSimpleName(), processPackages(pcks), getMappingFileNames(),
                    getProperties()
            );
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }

        Map<String, Object> configuration = new HashMap<>();
        configuration.put(org.hibernate.jpa.AvailableSettings.INTERCEPTOR, null);
        EntityManagerFactoryBuilderImpl entityManagerFactoryBuilder = new EntityManagerFactoryBuilderImpl(
                new PersistenceUnitInfoDescriptor(persistenceUnitInfo), configuration);
        entityManagerFactory =  entityManagerFactoryBuilder.build();
    }

    public  List<String> processPackages(List<String> pcks) {
            List<String> entities = new ArrayList<>();
            pcks.forEach(p -> {
                try {
                    entities.addAll(entityClassNames(p,getFolderEntity(p)));
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
            });
            return entities;
    }

    protected List<String> entityClassNames(String pack, String folderEntity) throws IOException, URISyntaxException {
        return entities(pack, folderEntity).stream().map(
                s ->{
                    try {
                        return this.getClass().getClassLoader().loadClass(s).getName();
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                    return null;
                }
        ).collect(Collectors.toList());
    }

    protected List<String> getMappingFileNames() throws IOException, URISyntaxException {
        return Arrays.asList(mappingFileNames());
    }


    protected Properties getProperties() {
        Properties properties = new Properties();
        properties.put("hibernate.dialect", "org.hibernate.dialect.PostgreSQL9Dialect");
        properties.put("hibernate.show_sql", Boolean.TRUE.toString());
        properties.put("hibernate.format_sql", Boolean.TRUE.toString());
        properties.put("javax.persistence.jdbc.url", "jdbc:postgresql://localhost:5432/prv_bizon_db");
        properties.put("javax.persistence.jdbc.user", "bizon");
        properties.put("javax.persistence.jdbc.driver", "org.postgresql.Driver");
        properties.put("javax.persistence.jdbc.password", "bizon");
        return properties;
    }

    public  String parseToPackage(String name) {
        String[] result = name.split("/");
        return Arrays.stream(result).collect(Collectors.joining("."));
    }


    public String getFolderEntity(String packagePath) {
        String[] arr = packagePath.split("/");
        return arr[arr.length-1]+"\\";
    }


    protected List<String> entities(String pack, String folderEntity) throws IOException, URISyntaxException {
        List<String> allEntities =  getAllItems(pack,folderEntity);
        Predicate<String> skipDollar = s -> s.contains("$");
        Predicate<String> skipFolders = s -> s.contains(".class");
        List<String> results = allEntities.stream().filter(skipFolders).filter(skipDollar.negate()).map(s ->{
            int toIndex = s.indexOf(".class");
            String nameOnly = s.substring(0,toIndex);
            String packageName = parseToPackage(pack);
            return packageName.concat(".").concat(nameOnly);
        }).collect(Collectors.toList());
        return results;
    }

    protected List<String> getAllItems(String pack,String folderEntity) throws URISyntaxException, IOException {
        URL url = MyAbstractPersistence.class.getClassLoader().getResource(pack);
        List<String> items = Files.list(Paths.get(url.toURI()))
                .map(s -> {
                    String itemPath = s.toString();
                    int domain = itemPath.lastIndexOf(folderEntity);
                    String entityName = itemPath.substring(domain);
                    return entityName.substring(folderEntity.length());
                }).collect(Collectors.toList());
        return items;
    }


    protected String[] mappingFileNames() throws IOException, URISyntaxException {

        return new String[] {
//                "META-INF/orm_query_employee_common.xml",
        };
    }

}
