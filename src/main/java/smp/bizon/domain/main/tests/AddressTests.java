package smp.bizon.domain.main.tests;

import smp.bizon.domain.bas.Address;
import smp.bizon.domain.bas.Geolocation;
import smp.bizon.domain.main.MyAbstractPersistence;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.List;

public class AddressTests extends MyAbstractPersistence {

    public static void main(String[] args) {
//        createAnAddress(args);
        findAllAdresses(args);
    }

    public static void findAllAdresses(String[] args) {

        EntityManager em = new AddressTests().entityManagerFactory.createEntityManager();
        em.getTransaction().begin();

        List<Address> addressList= em.createQuery("select a from Address a").getResultList();
        System.out.println(addressList);
        em.getTransaction().commit();
        em.close();

        System.exit(0);


    }

    public static void createAnAddress(String[] args) {
        EntityManager em = new AddressTests().entityManagerFactory.createEntityManager();
        em.getTransaction().begin();

        BigDecimal lat = BigDecimal.valueOf(1234.6789);
        BigDecimal lng = BigDecimal.valueOf(14.7589);

        Geolocation geolocation = Geolocation.builder()
                .lat(lat)
                .lng(lng)
                .build();
        Address address = Address.builder()
                .city("Genova")
                .geolocation(geolocation)
                .postalCode("5344 AM")
                .houseNumber("2")
                .build();
        em.persist(address);

        em.getTransaction().commit();
        em.close();
        System.exit(0);

    }
}
