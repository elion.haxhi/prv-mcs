package smp.bizon.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import smp.bizon.domain.bas.Address;

@Repository
public interface AddressRepository extends JpaRepository<Address,Long> {

    Address findByCityEquals(@Param(value = "city") String city);
}
