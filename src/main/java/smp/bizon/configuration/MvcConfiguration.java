package smp.bizon.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.CacheControl;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import java.net.URL;
import java.util.concurrent.TimeUnit;

@Configuration
@EnableWebMvc
@ComponentScan
public class MvcConfiguration extends WebMvcConfigurerAdapter {


    @Override
    public void addViewControllers(ViewControllerRegistry registry){

        registry.addViewController("/address/")
                .setViewName("forward:/address/index.html");

        registry.addViewController("/")
                //.setViewName("redirect:/seller/index.html");
                .setViewName("forward:/index.html");


    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {

        registry.addResourceHandler("/address/**")
                .addResourceLocations(getPathOfStaticFolder());
        registry.addResourceHandler("/**")
                .addResourceLocations(getPathOfStaticFolder());

//        registry.addResourceHandler("/**")
//                .addResourceLocations(
//                        "classpath:/prv-reactjs/")
//                .setCacheControl(CacheControl.noCache());

    }

    protected String getPathOfStaticFolder(){
        URL url = null;
        try {

            ClassLoader cLoader = MvcConfiguration.class.getClassLoader();

            url = cLoader.getResource("static");

        } catch (Exception e){
            e.printStackTrace();
        }

        // Get the relative path of static folder
        String s = url.getPath();
        int diff = s.indexOf("prv-mcs/");
        String root =s.substring(0,diff);
        String path ="file:" + root + "prv-mcs/src/main/resources/static/";


        return path;
    }

    @Override
    public void configureViewResolvers(ViewResolverRegistry registry) {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/WEB-INF/views/");
        resolver.setSuffix(".jsp");
        resolver.setViewClass(JstlView.class);
        registry.viewResolver(resolver);
    }



}
