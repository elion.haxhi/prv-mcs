package smp.bizon.dto;

public enum ResponseStatus {
	UNDEF,
	SUCCESS,
	ERROR,
	WARN
}
