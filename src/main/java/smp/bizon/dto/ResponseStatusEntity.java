package smp.bizon.dto;

public class ResponseStatusEntity<T> extends ResponseStatusDto{

	private T entity;

	public T getEntity() {
		return entity;
	}

	public void setEntity(T entity) {
		this.entity = entity;
	}
	
	
}
