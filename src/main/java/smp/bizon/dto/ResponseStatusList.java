package smp.bizon.dto;

import java.io.Serializable;
import java.util.List;

public class ResponseStatusList<T> extends ResponseStatusDto{

	private List<T> entities;

	public List<T> getEntities() {
		return entities;
	}

	public void setEntities(List<T> entities) {
		this.entities = entities;
	}
	
	
}
