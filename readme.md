# Microservice Project

Go with a console like cmd or gitbash under prv-mcs folder.\
Then execute this command:  mvn clean spring-boot:run
Go on http://localhost:8084/\
That's it.

Description:\
This is a very light-weight project.\
This is the way to go when you start building project from scratch.\

It exposes a web page and a rest service:\
<ol>
    <strong>Web</strong>
    <li>http://localhost:8084/</li>
    <strong>Rest</strong>
    <li>http:/localhost:8084/api/address/</li>
    <li>http:/localhost:8084/api/address/?city=someCity</li>
</ol>

For more info on rest open RestClient Software and import the project from:\
    
    ../prv-mcs-project/MyFolder/Development/prv-mcs/src/main/resources/restclient


**Architecture**

Class Model

![Class diagram](./src/main/resources/architecture/images/classdiagram.jpg)

Sequence all address

![Sequence diagram](./src/main/resources/architecture/images/address/sequence_getalladdress.jpg)

Sequence filter address

![Sequence diagram](./src/main/resources/architecture/images/address/sequence_filteraddress.jpg)

